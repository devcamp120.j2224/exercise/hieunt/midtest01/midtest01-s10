package com.devcamp.midtest01.school_classroom_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.midtest01.school_classroom_api.models.School;
import com.devcamp.midtest01.school_classroom_api.service.SchoolService;

@CrossOrigin
@RestController
public class SchoolController {
    // @Autowired
    // private static SchoolService schools;

    @GetMapping("/schools")
    public ArrayList<School> getAllSchools() {
        ArrayList<School> allSchools = SchoolService.getAllSchools();
        return allSchools;
    }

    @GetMapping("/school-info")
    public School getSchoolBySchoolId(@RequestParam(required = true) int schoolId) {
        ArrayList<School> allSchools = SchoolService.getAllSchools();
        School resultSchool = null;
        for (School bSchool : allSchools) {
            if(bSchool.getId() == schoolId) {
                resultSchool = bSchool;
            }
        }
        return resultSchool;
    }

    @GetMapping("/school-find")
    public ArrayList<School> getSchoolsByNoStudent(@RequestParam(required = true) int noNumber) {
        ArrayList<School> allSchools = SchoolService.getAllSchools();
        ArrayList<School> resultSchoolList = new ArrayList<>();
        for(School bSchool : allSchools) {
            if(bSchool.getTotalStudent() > noNumber) {
                resultSchoolList.add(bSchool);
            }
        }
        return resultSchoolList;
    }

}
