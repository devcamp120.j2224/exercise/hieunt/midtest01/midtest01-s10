package com.devcamp.midtest01.school_classroom_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.midtest01.school_classroom_api.models.Classroom;
import com.devcamp.midtest01.school_classroom_api.service.ClassroomService;

@RestController
@CrossOrigin
public class ClassroomController {
    // @Autowired
    // private ClassroomService classRoomService;

    @GetMapping("/classrooms")
    public ArrayList<Classroom> getAllClassrooms() {
        ArrayList<Classroom> allClassrooms = ClassroomService.getAllClassrooms();
        return allClassrooms;
    }

    @GetMapping("/classroom-find")
    public ArrayList<Classroom> getClassroomsByNoStudent(@RequestParam(required = true) int noNumber) {
        ArrayList<Classroom> allClassrooms = ClassroomService.getAllClassrooms();
        ArrayList<Classroom> resultClassroomList = new ArrayList<>();
        for(Classroom bClassroom : allClassrooms){
            if(bClassroom.getNoStudent() > noNumber) {
                resultClassroomList.add(bClassroom);
            }
        }
        return resultClassroomList;
    }
}
