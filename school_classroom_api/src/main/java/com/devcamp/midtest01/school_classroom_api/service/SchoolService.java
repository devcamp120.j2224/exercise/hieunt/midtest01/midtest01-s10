package com.devcamp.midtest01.school_classroom_api.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.midtest01.school_classroom_api.models.School;

@Service
public class SchoolService {
    private static ArrayList<School> allSchools = new ArrayList<>();

    // @Autowired
    // private static ClassroomService classrooms;

    static {
        School school1 = new School(1, "Tieu La", "Da Nang", ClassroomService.getClassroomList1());
        School school2 = new School(2, "Tran Phu", "Ha Noi", ClassroomService.getClassroomList2());
        School school3 = new School(3, "Quang Trung", "Ho Chi Minh", ClassroomService.getClassroomList3());
        allSchools.add(school1);
        allSchools.add(school2);
        allSchools.add(school3);
    }

    public static ArrayList<School> getAllSchools() {
        return allSchools;
    }
    public static void setAllSchools(ArrayList<School> allSchools) {
        SchoolService.allSchools = allSchools;
    }
}
