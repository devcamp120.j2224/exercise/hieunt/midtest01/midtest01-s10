package com.devcamp.midtest01.school_classroom_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.midtest01.school_classroom_api.models.Classroom;

@Service
public class ClassroomService {
    private static ArrayList<Classroom> classroomList1 = new ArrayList<>();
    private static ArrayList<Classroom> classroomList2 = new ArrayList<>();
    private static ArrayList<Classroom> classroomList3 = new ArrayList<>();
    private static ArrayList<Classroom> allClassrooms = new ArrayList<>();
    static {
        Classroom classroom101 = new Classroom(101, "A01", 50);
        Classroom classroom102 = new Classroom(102, "A02", 55);
        Classroom classroom103 = new Classroom(103, "A03", 45);
        Classroom classroom201 = new Classroom(201, "B01", 52);
        Classroom classroom202 = new Classroom(202, "B02", 60);
        Classroom classroom203 = new Classroom(203, "B03", 47);    
        Classroom classroom301 = new Classroom(301, "C01", 57);
        Classroom classroom302 = new Classroom(302, "C02", 62);
        Classroom classroom303 = new Classroom(303, "C03", 51);
        classroomList1.add(classroom101);
        classroomList1.add(classroom102);
        classroomList1.add(classroom103);
        classroomList2.add(classroom201);
        classroomList2.add(classroom202);
        classroomList2.add(classroom203);
        classroomList3.add(classroom301);
        classroomList3.add(classroom302);
        classroomList3.add(classroom303);
        for(Classroom bClassroom : classroomList1) {
            allClassrooms.add(bClassroom);
        }
        for(Classroom bClassroom : classroomList2) {
            allClassrooms.add(bClassroom);
        }
        for(Classroom bClassroom : classroomList3) {
            allClassrooms.add(bClassroom);
        }
    }
    public ClassroomService() {
    }
    public static ArrayList<Classroom> getClassroomList1() {
        return classroomList1;
    }
    public static void setClassroomList1(ArrayList<Classroom> classroomList1) {
        ClassroomService.classroomList1 = classroomList1;
    }
    public static ArrayList<Classroom> getClassroomList2() {
        return classroomList2;
    }
    public static void setClassroomList2(ArrayList<Classroom> classroomList2) {
        ClassroomService.classroomList2 = classroomList2;
    }
    public static ArrayList<Classroom> getClassroomList3() {
        return classroomList3;
    }
    public static void setClassroomList3(ArrayList<Classroom> classroomList3) {
        ClassroomService.classroomList3 = classroomList3;
    }
    public static ArrayList<Classroom> getAllClassrooms() {
        return allClassrooms;
    }
}
